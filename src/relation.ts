import UserAssetModel from "@shared/models/user_asset.model";
import EventModel from "@shared/models/event.model";
import AssetModel from "@shared/models/asset.model";
import CategoryModel from "@shared/models/category.model";
import FollowModel from "./shared/models/follow.model";
import UserModel from "./shared/models/user.model";
import CollectionModel from "@shared/models/collection.model";

/**
 * @Dev Follow
 */
FollowModel.belongsTo(UserModel, {
  foreignKey: "follower_id",
  onDelete: "CASCADE",
  onUpdate: "RESTRICT",
});

UserModel.hasMany(FollowModel, {
  foreignKey: "follower_id",
});

FollowModel.belongsTo(UserModel, {
  foreignKey: "following_id",
  onDelete: "CASCADE",
  onUpdate: "RESTRICT",
});

UserModel.hasMany(FollowModel, {
  foreignKey: "following_id",
});
//------------------------------------------------------------

/*
  @dev: category-asset
*/
AssetModel.belongsTo(CategoryModel, {
  foreignKey: "category_id",
  onDelete: "CASCADE",
  onUpdate: "RESTRICT",
});

CategoryModel.hasMany(AssetModel, {
  foreignKey: "category_id",
});
//------------------------------------------------------------

/*
  @dev: user-asset
*/
AssetModel.belongsTo(UserModel, {
  foreignKey: "creator_id",
  onDelete: "CASCADE",
  onUpdate: "RESTRICT",
});

UserModel.hasMany(AssetModel, {
  foreignKey: "user_id",
});
//------------------------------------------------------------

/*
  @dev: asset-event
*/
EventModel.belongsTo(AssetModel, {
  foreignKey: "asset_id",
  onDelete: "CASCADE",
  onUpdate: "RESTRICT",
});

AssetModel.hasMany(EventModel, {
  foreignKey: "asset_id",
});
//------------------------------------------------------------

/*
  @dev: userasset with asset, category, user
*/
UserAssetModel.belongsTo(AssetModel, {
  foreignKey: "asset_id",
  onDelete: "CASCADE",
  onUpdate: "RESTRICT",
});

UserAssetModel.belongsTo(CategoryModel, {
  foreignKey: "collection_id",
  onDelete: "CASCADE",
  onUpdate: "RESTRICT",
});

UserAssetModel.belongsTo(UserModel, {
  foreignKey: "user_id",
  onDelete: "CASCADE",
  onUpdate: "RESTRICT",
});

UserModel.hasMany(UserAssetModel, {
  foreignKey: "user_id",
});

CategoryModel.hasMany(UserAssetModel, {
  foreignKey: "collection_id",
});

AssetModel.hasMany(UserAssetModel, {
  foreignKey: "asset_id",
});

AssetModel.belongsTo(CollectionModel, {
  foreignKey: "collection_id",
  onDelete: "CASCADE",
  onUpdate: "RESTRICT",
});
CollectionModel.hasMany(AssetModel, {
  foreignKey: "collection_id",
});
//------------------------------------------------------------
