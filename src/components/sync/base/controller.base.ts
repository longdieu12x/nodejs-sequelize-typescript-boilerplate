interface IBaseControllerInterface {
  listen: () => Promise<void>;
}

export { IBaseControllerInterface };
