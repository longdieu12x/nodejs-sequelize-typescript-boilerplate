import { IAssetModel } from "../../../shared/models/asset.model";
import { getSocketProvider } from "../../../shared/utils/wallet.util";
import rpc from "../../../shared/constants/rpc";
import Padi1155 from "../../../shared/constants/contracts/Padi1155.sol/Padi1155.json";
import objAddress from "../../../shared/constants/contracts";
import { ethers } from "ethers";
import SyncRepository from "../sync.repository";
import { SyncType } from "../sync.enums";
import SyncService from "../sync.service";
import { IBaseControllerInterface } from "../base/controller.base";

class Padi1155Controller implements IBaseControllerInterface {
  private syncService = new SyncService();
  async listen() {
    const provider = getSocketProvider("ws://127.0.0.1:8545/");
    const nftContract = new ethers.Contract(
      objAddress["localhost"].Padi1155Address,
      Padi1155.abi,
      provider
    );
    provider.on("block", () => {
      console.log("hi");
    });
    nftContract.on("Mint", async (...args) => {
      console.log("---------------------------------------------------");
      console.log("NFT Minted: ");
      console.log(args);
      try {
        await this.syncService.updateAsset({
          nft_address: args[2],
          token_id: parseInt(args[3]),
          amount: parseInt(args[4]),
          ipfs: args[5],
        });

        const data = (await this.syncService.findAsset(
          args[2],
          parseInt(args[3])
        )) as IAssetModel;

        await this.syncService.createEvent({
          from: args[0],
          to: args[1],
          amount: parseInt(args[4]),
          tx_hash: args[6].transactionHash,
          price: undefined,
          type_user: SyncType.Minted,
          asset_id: data.id,
        });

        await this.syncService.createUserAsset({
          asset_id: data.id,
          collection_id: data.collection_id,
          user_id: data.creator_id,
          amount: parseInt(args[4]),
        });
      } catch (error: any) {
        console.log("Mint event error: ", error);
      }
    });
  }
}

export default Padi1155Controller;
