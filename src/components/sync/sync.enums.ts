enum SyncType {
  Created,
  Minted,
  Listing,
  Transfer,
  openAuction,
  placeBid,
  closeAuction,
}
export { SyncType };
