import { Expose } from "class-transformer";
import {
  Allow,
  IsEmpty,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
} from "class-validator";

export class CreateSyncDto {
  @Expose()
  @IsNotEmpty()
  @IsString()
  tx_hash: string;

  @Expose()
  @IsNotEmpty()
  @IsString()
  from: string;

  @Expose()
  @IsNotEmpty()
  @IsString()
  to: string;

  @Expose()
  @IsNotEmpty()
  @IsNumber()
  amount: number;

  @Expose()
  @IsOptional()
  @IsNumber()
  type_user: number;

  @Expose()
  @IsOptional()
  @IsNumber()
  price: number | undefined;

  @Expose()
  @IsOptional()
  @IsNumber()
  asset_id: number;
}

export class CreateAssetDto {
  @Expose()
  @IsNotEmpty()
  @IsString()
  nft_address: string;

  @Expose()
  @IsNotEmpty()
  @IsNumber()
  token_id: number;

  @Expose()
  @IsNotEmpty()
  @IsNumber()
  amount: number;

  @Expose()
  @IsNotEmpty()
  @IsString()
  ipfs: string;
}

export class CreateUserAssetDto {
  @Expose()
  @IsNotEmpty()
  @IsNumber()
  asset_id: number;

  @Expose()
  @IsNotEmpty()
  @IsNumber()
  collection_id: number;

  @Expose()
  @IsNotEmpty()
  @IsNumber()
  user_id: number;

  @Expose()
  @IsNotEmpty()
  @IsNumber()
  amount: number;
}
