const { ethers } = require("ethers");
const Padi721 = require("../../shared/constants/contracts/Padi721.sol/Padi721.json");
const Padi1155 = require("../../shared/constants/contracts/Padi1155.sol/Padi1155.json");

require("dotenv").config();

const main = async () => {
  const provider = new ethers.providers.JsonRpcProvider(
    "http://127.0.0.1:8545"
  );
  const signer = new ethers.Wallet(process.env.PRIVATE_KEY, provider);
  const nftContract = new ethers.Contract(
    "0x4C2F7092C2aE51D986bEFEe378e50BD4dB99C901",
    Padi721.abi,
    signer
  );
  let tx = await nftContract.approveMintItem(await signer.getAddress(), true);
  await tx.wait();
  await nftContract.safeMint("ipfs://asset-2");
};

main();
