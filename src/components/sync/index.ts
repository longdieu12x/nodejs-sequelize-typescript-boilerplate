import Padi1155Controller from "./controllers/padi1155.controller";
import Padi721Controller from "./controllers/padi721.controller";

const main = async () => {
  const padi721Controller = new Padi721Controller();
  const padi1155Controller = new Padi1155Controller();
  await Promise.all([padi721Controller.listen(), padi1155Controller.listen()]);
};

main();
