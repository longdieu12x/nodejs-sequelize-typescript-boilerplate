import {
  CreateSyncDto,
  CreateAssetDto,
  CreateUserAssetDto,
} from "./dtos/create.dto";
import SyncRepository from "./sync.repository";

class SyncService {
  private syncRepository = new SyncRepository();
  async findAsset(nft_address: string, token_id: number) {
    return await this.syncRepository.findAsset(nft_address, token_id);
  }

  async createEvent(createDto: CreateSyncDto) {
    await this.syncRepository.createEvent(createDto);
  }

  async updateAsset(createDto: CreateAssetDto) {
    await this.syncRepository.updateAsset(createDto);
  }

  async createUserAsset(createDto: CreateUserAssetDto) {
    await this.syncRepository.createUserAsset(createDto);
  }
}

export default SyncService;
