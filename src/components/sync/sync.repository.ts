import UserAssetModel from "@shared/models/user_asset.model";
import {
  CreateSyncDto,
  CreateAssetDto,
  CreateUserAssetDto,
} from "./dtos/create.dto";
import EventModel from "@shared/models/event.model";
import AssetModel from "@shared/models/asset.model";

class SyncRepository {
  private eventModel = EventModel;
  private assetModel = AssetModel;
  private userAssetModel = UserAssetModel;
  async createEvent(createDto: CreateSyncDto) {
    await this.eventModel.create(createDto);
  }
  // actually it is update, but test create now
  async updateAsset(createDto: CreateAssetDto) {
    console.log("Dto", createDto);
    const data = await this.assetModel.findOne({
      where: {
        nft_address: createDto.nft_address,
        ipfs: createDto.ipfs,
      },
      attributes: { exclude: ["user_id"] },
      raw: true,
    });
    console.log(data);
    await this.assetModel.update(createDto, {
      where: {
        id: data.id,
      },
    });
  }

  async findAsset(nft_address: string, token_id: number) {
    return await this.assetModel.findOne({
      where: {
        nft_address,
        token_id,
      },
      attributes: { exclude: ["user_id"] },
      raw: true,
    });
  }

  async createUserAsset(createDto: CreateUserAssetDto) {
    const { asset_id, collection_id, user_id } = createDto;
    const [user, created] = await this.userAssetModel.findOrCreate({
      where: {
        asset_id,
        collection_id,
        user_id,
      },
      defaults: {
        ...createDto,
      },
    });
    if (!created) {
      user.amount = createDto.amount;
      await user.save();
    }
  }
}

export default SyncRepository;
