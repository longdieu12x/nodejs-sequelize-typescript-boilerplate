import {
  Body,
  Post,
  JsonController,
  BadRequestError,
  NotFoundError,
  Put,
  Get,
  Param,
  Authorized,
  CurrentUser,
  Delete,
} from "routing-controllers";
import UserAssetService from "./user_asset.service";

@JsonController("/user-assets")
class UserAssetController {
  private userAssetService = new UserAssetService();

  @Get("/collections/:collection_id")
  async getAllAssetsFromCollection(
    @Param("collection_id") collection_id: number
  ) {
    try {
      return await this.userAssetService.getAllAssetsFromCollection(
        collection_id
      );
    } catch (error: any) {
      throw new BadRequestError(error.message);
    }
  }

  @Get("users/:user_id")
  async getAllAssetsFromUser(@Param("user_id") user_id: number) {
    try {
      return await this.userAssetService.getAllAssetsFromUser(user_id);
    } catch (error: any) {
      throw new BadRequestError(error.message);
    }
  }
}

export default UserAssetController;
