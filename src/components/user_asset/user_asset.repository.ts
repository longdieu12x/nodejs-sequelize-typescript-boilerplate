import UserAssetModel from "@shared/models/user_asset.model";

class UserAssetRepository {
  private userAssetRepository = UserAssetModel;

  async getAllAssetsFromCollection(collection_id: number) {
    return await this.userAssetRepository.findAll({
      where: {
        collection_id,
      },
    });
  }

  async getAllAssetsFromUser(user_id: number) {
    return await this.userAssetRepository.findAll({
      where: {
        user_id,
      },
    });
  }
}

export default UserAssetRepository;
