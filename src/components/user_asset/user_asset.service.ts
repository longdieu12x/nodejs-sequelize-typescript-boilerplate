import UserAssetRepository from "./user_asset.repository";
class UserAssetService {
  private userAssetRepository = new UserAssetRepository();

  async getAllAssetsFromCollection(collection_id: number) {
    return await this.userAssetRepository.getAllAssetsFromCollection(
      collection_id
    );
  }

  async getAllAssetsFromUser(user_id: number) {
    return await this.userAssetRepository.getAllAssetsFromUser(user_id);
  }
}

export default UserAssetService;
