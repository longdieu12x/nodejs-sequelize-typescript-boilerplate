import { IUserModel } from "./../../shared/models/user.model";
import {
  Body,
  Post,
  JsonController,
  BadRequestError,
  NotFoundError,
  Put,
  Get,
  Param,
  Authorized,
  CurrentUser,
  ForbiddenError,
  Delete,
  QueryParam,
} from "routing-controllers";
import CategoryService from "./category.service";
import { CreateCategoryDto } from "./dtos/create.dto";
import { UpdateCategoryDto } from "./dtos/update.dto";
import { UserType } from "../../components/user/user.enums";

@JsonController("/categories")
class CategoryController {
  private categoryService = new CategoryService();
  @Get("/")
  async getAllCategories() {
    try {
      return await this.categoryService.getAllCategories();
    } catch (error: any) {
      throw new BadRequestError(error.message);
    }
  }

  @Authorized()
  @Post("/")
  async create(
    @Body() createDto: CreateCategoryDto,
    @CurrentUser() user: IUserModel
  ) {
    try {
      if (user.type_user != UserType.Admin) {
        throw new ForbiddenError("You are not admin");
      }
      return await this.categoryService.create(createDto);
    } catch (error: any) {
      throw new BadRequestError(error.message);
    }
  }

  @Authorized()
  @Put("/:category_id")
  async update(
    @Body() updateDto: UpdateCategoryDto,
    @Param("category_id") category_id: number,
    @CurrentUser() user: IUserModel
  ) {
    try {
      if (user.type_user != UserType.Admin) {
        throw new ForbiddenError("You are not admin");
      }
      return await this.categoryService.update(updateDto, category_id);
    } catch (error: any) {
      throw new BadRequestError(error.message);
    }
  }

  @Authorized()
  @Delete("/:category_id")
  async delete(
    @Param("category_id") category_id: number,
    @CurrentUser() user: IUserModel
  ) {
    try {
      if (user.type_user != UserType.Admin) {
        throw new ForbiddenError("You are not admin");
      }
      return await this.categoryService.delete(category_id);
    } catch (error: any) {
      throw new BadRequestError(error.message);
    }
  }
}

export default CategoryController;
