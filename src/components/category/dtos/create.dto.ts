import { Expose } from "class-transformer";
import { Allow, IsNotEmpty, IsNumber, IsString } from "class-validator";

export class CreateCategoryDto {
  @Expose()
  @IsNotEmpty()
  @IsString()
  name: string;
}
