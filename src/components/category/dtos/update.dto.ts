import { Expose } from "class-transformer";
import { Allow, IsNotEmpty, IsNumber, IsString } from "class-validator";

export class UpdateCategoryDto {
  @Expose()
  @IsNotEmpty()
  @IsString()
  name: string;
}
