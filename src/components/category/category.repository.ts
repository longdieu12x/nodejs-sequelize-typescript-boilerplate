import CategoryModel from "../../shared/models/category.model";

class CategoryRepository {
  private categoryModel = CategoryModel;

  async getAll() {
    return await this.categoryModel.findAndCountAll({
      raw: true,
    });
  }

  async create(name: string) {
    await this.categoryModel.create({
      name,
    });
    return {
      message: "Create category successfully",
    };
  }

  async update(category_id: number, name: string) {
    await this.categoryModel.update(
      {
        name,
      },
      {
        where: {
          id: category_id,
        },
      }
    );
    return {
      message: "Update category successfully",
    };
  }

  async delete(category_id: number) {
    await this.categoryModel.destroy({
      where: {
        id: category_id,
      },
    });

    return {
      message: "Delete category successfully",
    };
  }
}

export default CategoryRepository;
