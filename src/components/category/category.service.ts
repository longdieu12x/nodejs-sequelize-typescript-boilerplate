import CategoryRepository from "./category.repository";
import { CreateCategoryDto } from "./dtos/create.dto";
import { UpdateCategoryDto } from "./dtos/update.dto";

class CategoryService {
  private categoryRepository = new CategoryRepository();

  async getAllCategories() {
    return await this.categoryRepository.getAll();
  }

  async create(createDto: CreateCategoryDto) {
    return await this.categoryRepository.create(createDto.name);
  }

  async update(updateDto: UpdateCategoryDto, category_id: number) {
    return await this.categoryRepository.update(category_id, updateDto.name);
  }

  async delete(category_id: number) {
    return await this.categoryRepository.delete(category_id);
  }
}

export default CategoryService;
