import { Expose } from "class-transformer";
import { Allow, IsNotEmpty, IsNumber, IsString } from "class-validator";

export class LoginDto {
  @Expose()
  @IsNotEmpty()
  @IsString()
  address: string;

  @Expose()
  @IsNotEmpty()
  @IsString()
  signature: string;
}
