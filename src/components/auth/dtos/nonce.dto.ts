import { Expose } from "class-transformer";
import { Allow, IsNotEmpty, IsNumber, IsString } from "class-validator";

export class NonceDto {
  @Expose()
  @IsNotEmpty()
  @IsString()
  address: string;
}
