import { Expose } from "class-transformer";
import { Allow, IsNotEmpty, IsNumber, IsString } from "class-validator";

export class RefreshDto {
  @Expose()
  @IsNotEmpty()
  @IsString()
  accessToken: string;

  @Expose()
  @IsNotEmpty()
  @IsString()
  refreshToken: string;
}
