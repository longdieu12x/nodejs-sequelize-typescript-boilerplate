import { IUserModel } from "./../../shared/models/user.model";
import { NonceDto } from "./dtos/nonce.dto";
import { LoginDto } from "./dtos/login.dto";
import {
  Body,
  Post,
  JsonController,
  BadRequestError,
  NotFoundError,
  Put,
  Get,
  Param,
  Authorized,
  CurrentUser,
  ForbiddenError,
} from "routing-controllers";
import AuthService from "./auth.service";
import { RefreshDto } from "./dtos/refresh.dto";

@JsonController("/auth")
class AuthController {
  private authService = new AuthService();
  @Post("/login")
  async login(@Body() loginDto: LoginDto) {
    return await this.authService.login(loginDto);
  }

  @Get("/nonce")
  async nonce(@Body() nonceDto: NonceDto) {
    try {
      return await this.authService.nonce(nonceDto);
    } catch (err: any) {
      throw new BadRequestError(err.message);
    }
  }

  @Post("/refresh")
  async refresh(@Body() refreshDto: RefreshDto) {
    try {
      return await this.authService.refresh(refreshDto);
    } catch (err: any) {
      throw new BadRequestError(err.message);
    }
  }

  @Authorized()
  async logout(@CurrentUser() user: IUserModel) {
    try {
      const address = user.address;
      return await this.authService.logout(address);
    } catch (err: any) {
      throw new ForbiddenError(err.message);
    }
  }
}

export default AuthController;
