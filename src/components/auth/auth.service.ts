import { NonceDto } from "./dtos/nonce.dto";
import { IUserModel } from "./../../shared/models/user.model";
import { LoginDto } from "./dtos/login.dto";
import AuthRepository from "./auth.repository";
import loginMessage from "../../shared/utils/loginMessage";
import { ethers } from "ethers";
import { NotFoundError } from "routing-controllers";
import random from "../../shared/utils/random";
import {
  createAccessToken,
  createRefreshToken,
  verifyToken,
} from "../../shared/utils/token";
import {
  deleteCacheExpire,
  getCacheExpire,
  setCacheExpire,
} from "../../shared/services/redis.service";
import { RefreshDto } from "./dtos/refresh.dto";
import { IAccessToken } from "../../shared/utils/interfaces/token.interface";
import { REFRESH_TTL } from "../../shared/constants/ttl";

class AuthService {
  private authRepository = new AuthRepository();

  async login(loginDto: LoginDto) {
    const { address, signature } = loginDto;

    const data = await this.authRepository.getUserByAddress(address);

    const user = data[0];
    if (data[1] == true) {
      // Create a new collection for new user
      await this.authRepository.createCollection(
        `Collection #${data[0].id}`,
        data[0].id
      );
    }

    let nonce = user.nonce;
    const message = loginMessage(address, nonce);
    const verifyAddress = ethers.utils.verifyMessage(message, signature);

    if (verifyAddress.toUpperCase() == address.toUpperCase()) {
      let nonce = random().toString();
      await this.authRepository.updateNonce(address, nonce);
      const accessToken = createAccessToken(user);
      const refreshToken = createRefreshToken(user);

      // Save to redis
      setCacheExpire(
        `auth_refresh_address_${address}`,
        refreshToken,
        REFRESH_TTL
      );

      return {
        accessToken,
        refreshToken,
      };
    }
  }

  async nonce(nonceDto: NonceDto) {
    const data = await this.authRepository.getUserByAddress(nonceDto.address);
    return data[0]?.nonce;
  }

  async refresh(refreshDto: RefreshDto) {
    const { accessToken, refreshToken } = refreshDto;
    const decryptAccess = (await verifyToken(accessToken)) as IAccessToken;
    const address = decryptAccess.address;
    const oldRefresh = await getCacheExpire(`auth_refresh_address_${address}`);
    if (
      JSON.parse(oldRefresh?.toLowerCase() as string) ==
      refreshToken.toLowerCase()
    ) {
      const data = await this.authRepository.getUserByAddress(address);
      const user = data[0];
      if (!user) {
        throw new NotFoundError("Not found user");
      }
      const newAccessToken = createAccessToken(user);
      const newRefreshToken = createRefreshToken(user);

      // set refresh to redis
      setCacheExpire(
        `auth_refresh_address_${address}`,
        newRefreshToken,
        REFRESH_TTL
      );

      return {
        accessToken: newAccessToken,
        refreshToken: newRefreshToken,
      };
    } else {
      throw new NotFoundError("Wrong token credentials!");
    }
  }

  async logout(address: string) {
    await deleteCacheExpire(`auth_refresh_address_${address}`);
    return {
      message: "Logout successfully",
    };
  }
}

export default AuthService;
