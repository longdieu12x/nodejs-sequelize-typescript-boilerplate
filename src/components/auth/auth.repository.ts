import CollectionModel from "@shared/models/collection.model";
import { Op } from "sequelize";
import UserModel from "../../shared/models/user.model";

class AuthRepository {
  private userModel = UserModel;
  private collectionModel = CollectionModel;

  async updateNonce(address: string, nonce: string) {
    return await this.userModel.update(
      {
        nonce,
      },
      {
        where: {
          address,
        },
      }
    );
  }

  async getUserByAddress(address: string) {
    return await this.userModel.findOrCreate({
      where: {
        address: {
          [Op.eq]: address,
        },
      },
      raw: true,
      defaults: {
        address,
      },
    });
  }

  async createCollection(title: string, user_id: number) {
    return await this.collectionModel.create({
      title,
      user_id,
    });
  }
}

export default AuthRepository;
