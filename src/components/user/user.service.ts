import { IUserModel } from "./../../shared/models/user.model";
import { instanceToPlain, classToPlain, plainToClass } from "class-transformer";
import { updateProfileDto } from "./dtos/updateProfile.dto";
import UserModel from "../../shared/models/user.model";
import UserRepository from "./user.repository";
import { Authorized, CurrentUser, ForbiddenError } from "routing-controllers";
import { UserType } from "./user.enums";

class UserService {
  private userRepository = new UserRepository();

  @Authorized()
  async updateProfile(
    updateProfile: updateProfileDto,
    @CurrentUser() user: IUserModel
  ) {
    const data = instanceToPlain(updateProfile) as updateProfileDto;
    return this.userRepository.update(data, user.id);
  }

  async getUserDetail(user: IUserModel) {
    const data = await this.userRepository.getById(user.id);
    return data;
  }
}

export default UserService;
