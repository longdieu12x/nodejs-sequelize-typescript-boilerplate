import { BadRequestError } from "routing-controllers";
import { Op } from "sequelize";
import UserModel from "../../shared/models/user.model";
import { updateProfileDto } from "./dtos/updateProfile.dto";

class UserRepository {
  private userModel = UserModel;

  async update(updateProfileDto: updateProfileDto, user_id: number) {
    const { ...data } = updateProfileDto;
    return this.userModel.update(
      { ...data },
      {
        where: {
          id: {
            [Op.eq]: user_id,
          },
        },
      }
    );
  }

  async getById(user_id: number) {
    const data = await this.userModel.findOne({
      where: {
        id: user_id,
      },
      raw: true,
      attributes: { exclude: ["type_user", "nonce"] },
    });
    return data;
  }
}

export default UserRepository;
