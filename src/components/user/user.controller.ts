import { IUserModel } from "./../../shared/models/user.model";
import {
  Body,
  Post,
  JsonController,
  BadRequestError,
  NotFoundError,
  Put,
  Get,
  Param,
  Authorized,
  CurrentUser,
} from "routing-controllers";
import UserService from "./user.service";
import { updateProfileDto } from "./dtos/updateProfile.dto";

@JsonController("/users")
class UserController {
  private userService = new UserService();

  @Authorized()
  @Put("/update")
  async updateProfile(
    @Body() updateProfileDto: updateProfileDto,
    @CurrentUser() user: IUserModel
  ) {
    try {
      return await this.userService.updateProfile(updateProfileDto, user);
    } catch (err: any) {
      throw new BadRequestError(err.message);
    }
  }

  @Authorized()
  @Get("/detail", { transformResponse: true })
  async getUserDetail(@CurrentUser() user: IUserModel) {
    try {
      return await this.userService.getUserDetail(user);
    } catch (err: any) {
      throw new BadRequestError(err.message);
    }
  }
}

export default UserController;
