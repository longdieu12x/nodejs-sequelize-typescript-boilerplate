import { IUserModel } from "./../../shared/models/user.model";
import {
  Body,
  Post,
  JsonController,
  BadRequestError,
  NotFoundError,
  Put,
  ForbiddenError,
  Get,
  Param,
  Authorized,
  CurrentUser,
  Patch,
  Delete,
} from "routing-controllers";
import FollowService from "./follow.service";

@JsonController("/follows")
class FollowController {
  private followService = new FollowService();

  @Authorized()
  @Post("/follow/:user_id")
  async follow(
    @Param("user_id") user_id: number,
    @CurrentUser() user: IUserModel
  ) {
    try {
      if (user_id == user.id) {
        throw new ForbiddenError("You can't follow yourself");
      }
      return await this.followService.follow(user_id, user);
    } catch (error: any) {
      throw new BadRequestError(error.message);
    }
  }

  @Authorized()
  @Delete("/unfollow/:user_id")
  async unfollow(
    @Param("user_id") user_id: number,
    @CurrentUser() user: IUserModel
  ) {
    try {
      if (user_id == user.id) {
        throw new ForbiddenError("You can't follow yourself");
      }
      return await this.followService.unfollow(user_id, user);
    } catch (error: any) {
      throw new BadRequestError(error.message);
    }
  }

  @Authorized()
  @Get("/")
  async getAllFollowings(@CurrentUser() user: IUserModel) {
    try {
      return await this.followService.getAllFollowings(user);
    } catch (error: any) {
      throw new BadRequestError(error.message);
    }
  }
}

export default FollowController;
