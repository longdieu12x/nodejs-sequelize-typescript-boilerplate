import UserModel from "../../shared/models/user.model";
import FollowModel from "../../shared/models/follow.model";
import { updateFollowUserDto } from "./dtos/updateFollowUser.dto";
class FollowRepository {
  private followModel = FollowModel;
  private userModel = UserModel;

  async createFollow(follower_id: number, following_id: number) {
    await this.followModel.create({
      follower_id,
      following_id,
    });
  }

  async deleteFollow(follower_id: number, following_id: number) {
    await this.followModel.destroy({
      where: {
        follower_id,
        following_id,
      },
    });
  }

  async updateFollowUsers(user_id: number, data: updateFollowUserDto) {
    await this.userModel.update(
      {
        ...data,
      },
      {
        where: {
          id: user_id,
        },
      }
    );
  }

  async getUserDetail(user_id: number) {
    return await this.userModel.findOne({
      where: {
        id: user_id,
      },
      raw: true,
    });
  }

  async getAllFollowings(user_id: number) {
    return await this.followModel.findAndCountAll({
      where: {
        follower_id: user_id,
      },
      include: {
        model: UserModel,
      },
      attributes: { exclude: ["follower_id"] },
      raw: true,
    });
  }
}

export default FollowRepository;
