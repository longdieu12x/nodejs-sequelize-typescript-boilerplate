import { IUserModel } from "./../../shared/models/user.model";
import FollowRepository from "./follow.repository";

class FollowService {
  private followRepository = new FollowRepository();

  async follow(following_id: number, user: IUserModel) {
    const follower_id = user.id as number;
    await this.followRepository.createFollow(follower_id, following_id);

    // update follow for following
    const followingHandle = async () => {
      let num_of_followings = user.num_of_followings + 1;
      let num_of_followers = user.num_of_followers;
      await this.followRepository.updateFollowUsers(follower_id, {
        num_of_followings,
        num_of_followers,
      });
    };

    // update follow for follower
    const followerHandle = async () => {
      const followingUser = (await this.followRepository.getUserDetail(
        following_id
      )) as IUserModel;
      let num_of_followings = followingUser.num_of_followings;
      let num_of_followers = followingUser.num_of_followers + 1;
      await this.followRepository.updateFollowUsers(following_id, {
        num_of_followings,
        num_of_followers,
      });
    };

    await Promise.all([followingHandle(), followerHandle()]);

    return {
      message: "Follow successfully",
    };
  }

  async unfollow(following_id: number, user: IUserModel) {
    const follower_id = user.id as number;
    await this.followRepository.deleteFollow(follower_id, following_id);

    // update follow for following
    let num_of_followings = user.num_of_followings - 1;
    let num_of_followers = user.num_of_followers;
    await this.followRepository.updateFollowUsers(follower_id, {
      num_of_followings,
      num_of_followers,
    });

    // update follow for follower
    const followingUser = (await this.followRepository.getUserDetail(
      following_id
    )) as IUserModel;
    num_of_followings = followingUser.num_of_followings;
    num_of_followers = followingUser.num_of_followers - 1;
    await this.followRepository.updateFollowUsers(following_id, {
      num_of_followings,
      num_of_followers,
    });

    return {
      message: "Unfollow successfully",
    };
  }

  async getAllFollowings(user: IUserModel) {
    const user_id = user.id;
    return await this.followRepository.getAllFollowings(user_id);
  }
}

export default FollowService;
