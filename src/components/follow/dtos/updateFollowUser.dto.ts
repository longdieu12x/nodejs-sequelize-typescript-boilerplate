import { Expose } from "class-transformer";
import { IsNumber, IsOptional } from "class-validator";

export class updateFollowUserDto {
  @Expose()
  @IsOptional()
  @IsNumber()
  num_of_followers: number;

  @Expose()
  @IsOptional()
  @IsNumber()
  num_of_followings: number;
}
