import { Expose } from "class-transformer";
import { IsNumber, IsOptional } from "class-validator";

export class follow {
  @Expose()
  @IsNumber()
  @IsOptional()
  follower_id: number;

  @Expose()
  @IsNumber()
  @IsOptional()
  following_id: number;
}
