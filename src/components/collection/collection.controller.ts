import { IUserModel } from "./../../shared/models/user.model";
import {
  Body,
  Post,
  JsonController,
  BadRequestError,
  NotFoundError,
  Put,
  Get,
  Param,
  Authorized,
  CurrentUser,
  Delete,
} from "routing-controllers";
import CollectionService from "./collection.service";
import { collectionDto } from "./dtos/create.dto";
import { updateCollectionDto } from "./dtos/update.dto";

@JsonController("/collections")
class CollectionController {
  private collectionService = new CollectionService();

  @Authorized()
  @Post("/create")
  async createCollection(
    @Body() collectionDto: collectionDto,
    @CurrentUser() user: IUserModel
  ) {
    try {
      let createProcess = this.collectionService.createCollection(
        collectionDto,
        user.id
      );
      let responseCreate = await createProcess;
      return responseCreate;
    } catch (err: any) {
      throw new BadRequestError(err.message);
    }
  }

  @Put("/update/:collection_id")
  async updateCollection(
    @Body() updateCollectionDto: updateCollectionDto,
    @Param("collection_id") collection_id: number
  ) {
    try {
      let updateProcess = this.collectionService.updateCollection(
        updateCollectionDto,
        collection_id
      );
      let responseUpdate = await updateProcess;
      return responseUpdate;
    } catch (err: any) {
      throw new BadRequestError(err.message);
    }
  }
}

export default CollectionController;
