import { Expose } from "class-transformer";
import {
  IsEmail,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
} from "class-validator";

export class updateCollectionDto {
  data: object;
  @Expose()
  @IsString()
  @IsOptional()
  image: string;

  @Expose()
  @IsString()
  @IsOptional()
  banner_img: string;

  @Expose()
  @IsString()
  @IsOptional()
  description: string;

  @Expose()
  @IsString()
  @IsOptional()
  title: string;
}
