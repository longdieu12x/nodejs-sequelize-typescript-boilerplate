import { IUserModel } from "./../../shared/models/user.model";
import { instanceToPlain, classToPlain, plainToClass } from "class-transformer";
import { collectionDto } from "./dtos/create.dto";
import CollectionRepository from "./collection.repository";
import { updateCollectionDto } from "./dtos/update.dto";
import { Authorized, CurrentUser } from "routing-controllers";

class CollectionService {
  private collectionRepository = new CollectionRepository();

  createCollection(collection: collectionDto, user_id: number) {
    const data = instanceToPlain(collection) as collectionDto;
    return this.collectionRepository.create(data, user_id);
  }

  updateCollection(
    updateCollectionDto: updateCollectionDto,
    collection_id: number
  ) {
    const data = instanceToPlain(updateCollectionDto) as updateCollectionDto;
    return this.collectionRepository.update(data, collection_id);
  }
}

export default CollectionService;
