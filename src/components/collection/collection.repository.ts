import { BadRequestError } from "routing-controllers";
import { Op } from "sequelize";
import CollectionModel from "../../shared/models/collection.model";
import { collectionDto } from "./dtos/create.dto";
import { updateCollectionDto } from "./dtos/update.dto";

class CollectionRepository {
  private collectionModel = CollectionModel;

  create(collectionDto: collectionDto, user_id: number) {
    const data = { ...collectionDto, user_id };
    return this.collectionModel.create(data);
  }

  update(updateCollectionDto: updateCollectionDto, collection_id: number) {
    const data = updateCollectionDto;
    return this.collectionModel.update(data, {
      where: {
        id: collection_id,
      },
    });
  }
}

export default CollectionRepository;
