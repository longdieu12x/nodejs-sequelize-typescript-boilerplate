import AssetModel from "../../shared/models/asset.model";
import EventModel from "../../shared/models/event.model";

class EventRepository {
  private eventModel = EventModel;
  private assetModel = AssetModel;
  async getAll() {
    return await this.eventModel.findAndCountAll({
      raw: true,
    });
  }
}

export default EventRepository;
