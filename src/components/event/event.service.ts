import EventRepository from "./event.repository";

class EventService {
  private eventRepository = new EventRepository();
  async getAll() {
    return await this.eventRepository.getAll();
  }
}

export default EventService;
