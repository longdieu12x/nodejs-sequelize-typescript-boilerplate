import {
  Body,
  Post,
  JsonController,
  BadRequestError,
  NotFoundError,
  Put,
  Get,
  Param,
  Authorized,
  CurrentUser,
  ForbiddenError,
} from "routing-controllers";
import EventService from "./event.service";

@JsonController("/events")
class EventController {
  private eventService = new EventService();
  @Get("/")
  async getAll() {
    try {
      return await this.eventService.getAll();
    } catch (error: any) {
      throw new BadRequestError(error.message);
    }
  }
}
export default EventController;
