import { IUserModel } from "./../../shared/models/user.model";
import { CreateDto } from "./dtos/create.dto";
import AssetRepository from "./asset.repository";

class AssetService {
  private assetRepository = new AssetRepository();

  async createAsset(createDto: CreateDto, user: IUserModel) {
    const collection_id = createDto.collection_id;
    const user_id = user.id;
    await this.assetRepository.checkUserHasCollection(collection_id, user_id);
    await this.assetRepository.createAsset(createDto, user);
    return {
      message: "Create asset successfully",
    };
  }

  async freezeAsset(asset_id: number, user: IUserModel) {
    /**
     * Upload metadata to ipfs
     * For simply I just fix it for test
     */
    const ipfs = "ipfs://asset-" + asset_id;
    await this.assetRepository.checkUserHasAsset(asset_id, user.id);
    await this.assetRepository.freezeAsset(asset_id, ipfs);
    return {
      message: "Freeze asset successfully",
    };
  }

  async viewAsset(asset_id: number) {
    return await this.assetRepository.viewAsset(asset_id);
  }

  async likeAsset(asset_id: number) {
    return await this.assetRepository.likeAsset(asset_id);
  }
}

export default AssetService;
