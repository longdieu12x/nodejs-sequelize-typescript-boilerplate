import { IUserModel } from "./../../shared/models/user.model";
import CollectionModel from "@shared/models/collection.model";
import obj from "@shared/constants/contracts";
import AssetModel from "../../shared/models/asset.model";
import { CreateDto } from "./dtos/create.dto";
import { Op } from "sequelize";
import { AssetType } from "./asset.enums";
class AssetRepository {
  private assetModel = AssetModel;
  private collectionModel = CollectionModel;
  async createAsset(createDto: CreateDto, user: IUserModel) {
    const data = {
      ...createDto,
      creator_id: user.id,
      creator_address: user.address,
      nft_address: (createDto.type = AssetType.ERC721)
        ? obj["localhost"].Padi721Address
        : obj["localhost"].Padi1155Address,
      num_of_edition: createDto.amount,
    };
    return await this.assetModel.create(data);
  }

  async checkUserHasCollection(collection_id: number, user_id: number) {
    return await this.collectionModel.findOne({
      where: {
        id: collection_id,
        user_id,
      },
    });
  }

  async checkUserHasAsset(asset_id: number, user_id: number) {
    console.log("User Id: ", user_id);
    return await this.assetModel.findOne({
      where: {
        id: {
          [Op.eq]: asset_id,
        },
        creator_id: {
          [Op.eq]: user_id,
        },
      },
      attributes: { exclude: ["user_id"] },
    });
  }

  async freezeAsset(asset_id: number, ipfs: string) {
    return await this.assetModel.update(
      {
        ipfs,
      },
      {
        where: {
          id: asset_id,
        },
      }
    );
  }

  async viewAsset(asset_id: number) {
    const data = await this.assetModel.findOne({
      where: {
        id: asset_id,
      },
      attributes: { exclude: ["user_id"] },
    });
    data.num_of_view += 1;
    await data.save();
  }

  async likeAsset(asset_id: number) {
    const data = await this.assetModel.findOne({
      where: {
        id: asset_id,
      },
      attributes: { exclude: ["user_id"] },
    });
    data.num_of_like += 1;
    await data.save();
  }
}

export default AssetRepository;
