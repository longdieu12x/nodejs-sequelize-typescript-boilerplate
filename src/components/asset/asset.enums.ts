enum AssetType {
  ERC721,
  ERC1155,
}

enum AssetStatus {
  LIST,
  UNLIST,
}

export { AssetType, AssetStatus };
