import { Expose } from "class-transformer";
import {
  Allow,
  IsBoolean,
  IsEmpty,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
} from "class-validator";

export class CreateDto {
  @Expose()
  @IsNotEmpty()
  @IsString()
  image: string;

  @Expose()
  @IsNotEmpty()
  @IsString()
  name: string;

  @Expose()
  @IsNotEmpty()
  @IsString()
  description: string;

  @Expose()
  @IsOptional()
  @IsString()
  external_link: string;

  @Expose()
  @IsNotEmpty()
  @IsNumber()
  amount: number;

  @Expose()
  @IsNotEmpty()
  @IsBoolean()
  is_hidden: boolean;

  @Expose()
  @IsNotEmpty()
  @IsBoolean()
  is_sensitive: boolean;

  @Expose()
  @IsOptional()
  @IsString()
  unlockable_content: string;

  @Expose()
  @IsNotEmpty()
  @IsNumber()
  type: number;

  @Expose()
  @IsNotEmpty()
  @IsNumber()
  category_id: number;

  @Expose()
  @IsNotEmpty()
  @IsNumber()
  collection_id: number;
}
