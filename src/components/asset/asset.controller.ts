import { IUserModel } from "./../../shared/models/user.model";
import { CreateDto } from "./dtos/create.dto";
import {
  Body,
  Post,
  JsonController,
  BadRequestError,
  NotFoundError,
  Patch,
  Put,
  Get,
  Param,
  Authorized,
  CurrentUser,
  ForbiddenError,
} from "routing-controllers";
import AssetService from "./asset.service";

@JsonController("/assets")
class AssetController {
  private assetService = new AssetService();

  @Authorized()
  @Post("/create")
  async createAsset(@Body() body: CreateDto, @CurrentUser() user: IUserModel) {
    try {
      return await this.assetService.createAsset(body, user);
    } catch (error: any) {
      throw new BadRequestError(error.message);
    }
  }

  @Authorized()
  @Patch("/freeze/:asset_id")
  async freezeAsset(
    @Param("asset_id") asset_id: number,
    @CurrentUser() user: IUserModel
  ) {
    try {
      return await this.assetService.freezeAsset(asset_id, user);
    } catch (error: any) {
      throw new BadRequestError(error.message);
    }
  }

  @Patch("/view/:asset_id")
  async viewAsset(@Param("asset_id") asset_id: number) {
    try {
      return await this.assetService.viewAsset(asset_id);
    } catch (error: any) {
      throw new NotFoundError(error.message);
    }
  }

  @Authorized()
  @Patch("/like/:asset_id")
  async likeAsset(@Param("asset_id") asset_id: number) {
    try {
      return await this.assetService.likeAsset(asset_id);
    } catch (error: any) {
      throw new NotFoundError(error.message);
    }
  }
}

export default AssetController;
