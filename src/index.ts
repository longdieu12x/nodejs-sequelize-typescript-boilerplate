import "module-alias/register";
import "reflect-metadata";
import express, { Request, Response } from "express";
import compression from "compression";
import cors from "cors";
import morgan from "morgan";
import helmet from "helmet";
import { validateEnv } from "./shared/utils/validate-env";
import { sequelize } from "./shared/models";
import { useExpressServer, Action } from "routing-controllers";
import "./relation";
import { verifyToken } from "./shared/utils/token";
import "./components/sync";
import { Op } from "sequelize";
import {
  IAccessToken,
  IRefreshToken,
} from "./shared/utils/interfaces/token.interface";
import UserController from "./components/user/user.controller";
import AuthController from "./components/auth/auth.controller";
import CollectionController from "./components/collection/collection.controller";
import { client as RedisClient } from "./shared/services/redis.service";
import FollowController from "./components/follow/follow.controller";
import UserModel from "./shared/models/user.model";
import CategoryController from "./components/category/category.controller";
import AssetController from "@components/asset/asset.controller";

const app = express();
validateEnv();

app.use(helmet());
app.use(cors());
app.use(morgan("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(
  compression({
    level: 6,
    threshold: 100 * 1000, // 100kb,
    filter: (req: Request, res: Response) => {
      if (req.headers["x-no-compression"]) {
        // don't compress responses with this request header
        return false;
      }
      return compression.filter(req, res);
    },
  })
);

sequelize
  .sync()
  .then(async () => {
    await RedisClient.connect();
    useExpressServer(app, {
      plainToClassTransformOptions: {
        excludeExtraneousValues: true,
      },
      validation: true,
      authorizationChecker: async (action: Action, roles: string[]) => {
        try {
          const token = action.request.headers["authorization"].split(" ")[1];
          await verifyToken(token);
          return true;
        } catch (err: any) {
          return false;
        }
      },
      currentUserChecker: async (action: Action) => {
        const token = action.request.headers["authorization"].split(" ")[1];
        const user = (await verifyToken(token)) as IAccessToken;
        try {
          return await UserModel.findOne({
            where: {
              address: user.address,
            },
            raw: true,
          });
        } catch (e) {
          return null;
        }
      },
      routePrefix: "/api",
      controllers: [
        UserController,
        AuthController,
        FollowController,
        CategoryController,
        CollectionController,
        AssetController,
      ], // and configure it the way you need (controllers, validation, etc.)
    }).listen(process.env.PORT);
    console.log("Express is running on PORT " + process.env.PORT);
  })
  .catch((err: any) => {
    console.log(err);
  });
