const obj = {
  localhost: {
    AutionAddress: "0x0165878A594ca255338adfa4d48449f69242Eb8F",
    MarketAddress: "0x2279B7A0a67DB372996a5FaB50D91eAA73d2eBe6",
    OfferAddress: "0x610178dA211FEF7D417bC0e6FeD39F05609AD788",
    Padi721Address: "0x4C2F7092C2aE51D986bEFEe378e50BD4dB99C901",
    Padi1155Address: "0x49fd2BE640DB2910c2fAb69bB8531Ab6E76127ff",
    WhitelistAddress: "0x959922bE3CAee4b8Cd9a407cc3ac1C251C2007B1",
    CurrencyAddress: "0x68B1D87F95878fE05B998F19b66F4baba5De1aed",
    DaiAddress: "0x3Aa5ebB10DC797CAC828524e59A333d0A371443c",
  },
};

export default obj;
