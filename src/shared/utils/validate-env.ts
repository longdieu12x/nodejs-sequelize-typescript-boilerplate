import { cleanEnv, str, port, num } from "envalid";

function validateEnv(): void {
  cleanEnv(process.env, {
    NODE_ENV: str({
      choices: ["development", "production"],
    }),
    DB_PASS: str(),
    DB_HOST: str(),
    DB_USER: str(),
    DB_NAME: str(),
    DB_PORT: str(),
    DB_DIALECT: str(),
    PRIVATE_KEY: str(),
    PORT: port({ default: 4000 }),
  });
}

export { validateEnv };
