import { IUserModel } from "./../models/user.model";
import { IAccessToken, IRefreshToken } from "./interfaces/token.interface";
import jwt from "jsonwebtoken";

const createAccessToken = (user: IUserModel): string => {
  return jwt.sign(
    {
      address: user.address,
    },
    process.env.JWT as jwt.Secret,
    {
      expiresIn: "3h",
    }
  );
};

const createRefreshToken = (user: IUserModel): string => {
  return jwt.sign(
    {
      id: user.id,
    },
    process.env.JWT as jwt.Secret,
    {
      expiresIn: "1d",
    }
  );
};

const verifyToken = async (
  token: string
): Promise<jwt.VerifyErrors | IAccessToken | IRefreshToken> => {
  return new Promise((resolve, reject) => {
    jwt.verify(token, process.env.JWT as jwt.Secret, (err, payload) => {
      if (err) return reject(err);
      resolve(payload as IAccessToken | IRefreshToken);
    });
  });
};

export { createAccessToken, createRefreshToken, verifyToken };
