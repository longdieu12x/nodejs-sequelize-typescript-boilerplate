const errorWrapper = async <T>(dbFunc: () => Promise<T>) => {
  let error = false;
  try {
    await dbFunc();
  } catch (err: any) {
    error = true;
  }
  return error;
};
export default errorWrapper;
