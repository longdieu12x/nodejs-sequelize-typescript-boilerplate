import { ethers } from "ethers";
import "dotenv/config";

const getProvider = (rpcUrl: string) => {
  return new ethers.providers.JsonRpcProvider(rpcUrl);
};

const getSocketProvider = (rpcUrl: string) => {
  return new ethers.providers.WebSocketProvider(rpcUrl);
};

const getSigner = (rpcUrl: string) => {
  return new ethers.Wallet(process.env.PRIVATE_KEY, getProvider(rpcUrl));
};

export { getProvider, getSigner, getSocketProvider };
