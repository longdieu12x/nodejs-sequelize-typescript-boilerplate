import {
  Model,
  DataTypes,
  CreationOptional,
  InferAttributes,
  InferCreationAttributes,
} from "sequelize";
import { sequelize } from "./index";
import UserModel from "./user.model";

export interface IFollowModel
  extends Model<
    InferAttributes<IFollowModel>,
    InferCreationAttributes<IFollowModel>
  > {
  follower_id: CreationOptional<number>;
  following_id: CreationOptional<number>;
}

const FollowModel = sequelize.define<IFollowModel>(
  "Follow",
  {
    follower_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
      onDelete: "CASCADE",
      onUpdate: "RESTRICT",
      references: {
        model: UserModel.tableName,
        key: "id",
      },
      primaryKey: true,
    },
    following_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
      onDelete: "CASCADE",
      onUpdate: "RESTRICT",
      references: {
        model: UserModel.tableName,
        key: "id",
      },
      primaryKey: true,
    },
  },
  {
    timestamps: true,
    underscored: true,
  }
);

export default FollowModel;
