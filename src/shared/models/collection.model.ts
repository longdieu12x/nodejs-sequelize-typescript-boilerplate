import {
  Model,
  DataTypes,
  CreationOptional,
  InferAttributes,
  InferCreationAttributes,
  ForeignKey,
} from "sequelize";
import { sequelize } from "./index";
import UserModel from "./user.model";

export interface ICollectionModel
  extends Model<
    InferAttributes<ICollectionModel>,
    InferCreationAttributes<ICollectionModel>
  > {
  id: CreationOptional<number>;
  image: CreationOptional<string>;
  banner_img: CreationOptional<string>;
  title: CreationOptional<string>;
  description: CreationOptional<string>;
  user_id: ForeignKey<number>;
  createdAt: CreationOptional<Date>;
  updatedAt: CreationOptional<Date>;
}

const CollectionModel = sequelize.define<ICollectionModel>(
  "Collection",
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      allowNull: false,
      primaryKey: true,
    },
    image: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    banner_img: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    title: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    description: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      onDelete: "CASCADE",
      onUpdate: "RESTRICT",
      references: {
        model: UserModel.tableName,
        key: "id",
      },
    },
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW,
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW,
    },
  },
  {
    timestamps: true,
  }
);

CollectionModel.belongsTo(UserModel, {
  foreignKey: "user_id",
});
UserModel.hasMany(CollectionModel, {
  foreignKey: "user_id",
});

export default CollectionModel;
