import {
  Model,
  DataTypes,
  CreationOptional,
  InferAttributes,
  InferCreationAttributes,
} from "sequelize";
import { UserType } from "../../components/user/user.enums";
import { sequelize } from "./index";

export interface IUserModel
  extends Model<
    InferAttributes<IUserModel>,
    InferCreationAttributes<IUserModel>
  > {
  id: CreationOptional<number>;
  name: CreationOptional<string>;
  description: CreationOptional<string>;
  email: CreationOptional<string>;
  country_id: CreationOptional<number>;
  address: string;
  nonce: string;
  num_of_followers: CreationOptional<number>;
  num_of_followings: CreationOptional<number>;
  profile_img: CreationOptional<string>;
  banner_img: CreationOptional<string>;
  facebook_url: CreationOptional<string>;
  instagram_url: CreationOptional<string>;
  youtube_url: CreationOptional<string>;
  twitter_url: CreationOptional<string>;
  language: CreationOptional<string>;
  type_user: number;
}

const UserModel = sequelize.define<IUserModel>(
  "User",
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      allowNull: false,
      primaryKey: true,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    description: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    email: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    country_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
    address: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
    },
    nonce: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: 0,
    },
    num_of_followers: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
    },
    num_of_followings: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
    },
    profile_img: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    banner_img: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    facebook_url: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    twitter_url: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    youtube_url: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    instagram_url: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    language: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: "en",
    },
    type_user: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: UserType.User,
    },
  },
  {
    timestamps: true,
  }
);

export default UserModel;
