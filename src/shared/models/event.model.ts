import AssetModel from "../../shared/models/asset.model";
import {
  Model,
  DataTypes,
  CreationOptional,
  InferAttributes,
  InferCreationAttributes,
  ForeignKey,
} from "sequelize";
import { sequelize } from "./index";

export interface IEventModel
  extends Model<
    InferAttributes<IEventModel>,
    InferCreationAttributes<IEventModel>
  > {
  id: CreationOptional<number>;
  tx_hash: string;
  from: string;
  to: string;
  amount: number;
  price: CreationOptional<number>;
  asset_id: ForeignKey<number>;
  type_user: number;
}

const EventModel = sequelize.define<IEventModel>(
  "Event",
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      allowNull: false,
      primaryKey: true,
    },
    tx_hash: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    from: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    to: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    amount: {
      type: DataTypes.INTEGER,
      defaultValue: 1,
    },
    price: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
    asset_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      onDelete: "CASCADE",
      onUpdate: "RESTRICT",
      references: {
        model: AssetModel.tableName,
        key: "id",
      },
    },
    type_user: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
  },
  {
    timestamps: true,
  }
);

export default EventModel;
