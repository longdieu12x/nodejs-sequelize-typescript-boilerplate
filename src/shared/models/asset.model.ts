import { AssetStatus, AssetType } from "../../components/asset/asset.enums";
import {
  Model,
  DataTypes,
  CreationOptional,
  InferAttributes,
  InferCreationAttributes,
  ForeignKey,
} from "sequelize";
import CategoryModel from "./category.model";
import { sequelize } from "./index";
import UserModel from "./user.model";
import CollectionModel from "./collection.model";

export interface IAssetModel
  extends Model<
    InferAttributes<IAssetModel>,
    InferCreationAttributes<IAssetModel>
  > {
  id: CreationOptional<number>;
  name: string;
  image: string;
  description: string;
  external_link: string;
  nft_address: string;
  token_id: number;
  amount: number;
  category_id: ForeignKey<number>;
  creator_address: string;
  creator_id: ForeignKey<number>;
  collection_id: ForeignKey<number>;
  edition: number;
  ipfs: string;
  metadata: string;
  unlockable_content: string;
  num_of_like: number;
  num_of_view: number;
  num_of_edition: number;
  num_of_claim: number;
  is_sentitive: boolean;
  is_hidden: boolean;
  type: number;
  status: number;
}

const AssetModel = sequelize.define<IAssetModel>(
  "Asset",
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      allowNull: false,
      primaryKey: true,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: "",
    },
    image: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: "",
    },
    description: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: "",
    },
    external_link: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: "",
    },
    nft_address: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: "",
    },
    token_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
    amount: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
    category_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      onDelete: "CASCADE",
      onUpdate: "RESTRICT",
      references: {
        model: CategoryModel.tableName,
        key: "id",
      },
    },
    collection_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      onDelete: "CASCADE",
      onUpdate: "RESTRICT",
      references: {
        model: CollectionModel.tableName,
        key: "id",
      },
    },
    creator_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      onDelete: "CASCADE",
      onUpdate: "RESTRICT",
      references: {
        model: UserModel.tableName,
        key: "id",
      },
    },
    creator_address: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    edition: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
    },
    ipfs: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: "",
    },
    metadata: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: "",
    },
    unlockable_content: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: "",
    },
    num_of_like: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
    },
    num_of_view: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
    },
    num_of_edition: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
    },
    num_of_claim: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
    },
    is_sentitive: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
    is_hidden: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
    type: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: AssetType.ERC721,
    },
    status: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: AssetStatus.UNLIST,
    },
  },
  {
    timestamps: true,
  }
);

export default AssetModel;
