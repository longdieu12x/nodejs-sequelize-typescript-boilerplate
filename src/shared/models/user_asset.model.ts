import CategoryModel from "../models/category.model";
import AssetModel from "../models/asset.model";
import UserModel from "./user.model";
import {
  Model,
  DataTypes,
  CreationOptional,
  InferAttributes,
  InferCreationAttributes,
  ForeignKey,
} from "sequelize";
import { sequelize } from "./index";

export interface IUserAssetModel
  extends Model<
    InferAttributes<IUserAssetModel>,
    InferCreationAttributes<IUserAssetModel>
  > {
  asset_id: ForeignKey<number>;
  collection_id: ForeignKey<number>;
  user_id: ForeignKey<number>;
  amount: number;
}

const UserAssetModel = sequelize.define<IUserAssetModel>(
  "UserAsset",
  {
    asset_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      onDelete: "CASCADE",
      onUpdate: "RESTRICT",
      references: {
        model: AssetModel.tableName,
        key: "id",
      },
    },
    collection_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      onDelete: "CASCADE",
      onUpdate: "RESTRICT",
      references: {
        model: CategoryModel.tableName,
        key: "id",
      },
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      onDelete: "CASCADE",
      onUpdate: "RESTRICT",
      references: {
        model: UserModel.tableName,
        key: "id",
      },
    },
    amount: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
  },
  {
    timestamps: true,
  }
);

export default UserAssetModel;
