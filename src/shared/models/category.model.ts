import {
  Model,
  DataTypes,
  CreationOptional,
  InferAttributes,
  InferCreationAttributes,
} from "sequelize";
import { sequelize } from "./index";

export interface ICategoryModel
  extends Model<
    InferAttributes<ICategoryModel>,
    InferCreationAttributes<ICategoryModel>
  > {
  id: CreationOptional<number>;
  name: string;
}

const CategoryModel = sequelize.define<ICategoryModel>(
  "Category",
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      allowNull: false,
      primaryKey: true,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: true,
    },
  },
  {
    timestamps: true,
  }
);

export default CategoryModel;
