import { Sequelize, Dialect } from "sequelize";
import "dotenv/config";
import config from "../../database/config/config";
const dbConfig = config;

const sequelize = new Sequelize(
  dbConfig.database,
  dbConfig.username,
  dbConfig.password,
  {
    host: dbConfig.host,
    port: parseInt(dbConfig.port),
    dialect: dbConfig.dialect as Dialect,
  }
);

export { Sequelize, sequelize };
