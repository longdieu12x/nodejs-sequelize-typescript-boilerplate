import { Dialect } from "sequelize";

export {};

declare global {
  namespace NodeJS {
    interface ProcessEnv {
      DB_PORT: string;
      DB_USER: string;
      DB_PASS: string;
      DB_HOST: string;
      DB_NAME: string;
      DB_DIALECT: Dialect;
      PRIVATE_KEY: string;
      NODE_ENV: "development" | "production";
      JWT: string;
    }
  }
}
