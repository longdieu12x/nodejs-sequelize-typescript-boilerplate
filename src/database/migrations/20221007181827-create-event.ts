import { DataTypes, QueryInterface } from "sequelize";
import EventModel from "../../shared/models/event.model";
import AssetModel from "../../shared/models/asset.model";

module.exports = {
  async up(queryInterface: QueryInterface) {
    await queryInterface.createTable(EventModel.tableName, {
      id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true,
      },
      tx_hash: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      from: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      to: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      amount: {
        type: DataTypes.INTEGER,
        defaultValue: 1,
      },
      price: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      asset_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        onDelete: "CASCADE",
        onUpdate: "RESTRICT",
        references: {
          model: AssetModel.tableName,
          key: "id",
        },
      },
      type_user: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      createdAt: { type: DataTypes.DATE, defaultValue: new Date() },
      updatedAt: { type: DataTypes.DATE, defaultValue: new Date() },
    });
  },

  async down(queryInterface: QueryInterface) {
    await queryInterface.dropTable(EventModel.tableName);
  },
};
