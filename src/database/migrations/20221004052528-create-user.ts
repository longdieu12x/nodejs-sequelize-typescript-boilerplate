import { DataTypes, QueryInterface } from "sequelize";
import { UserType } from "../../components/user/user.enums";
import UserModel from "../../shared/models/user.model";

module.exports = {
  async up(queryInterface: QueryInterface) {
    await queryInterface.createTable(UserModel.tableName, {
      id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true,
      },
      name: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      description: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      email: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      country_id: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      address: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
      },
      nonce: {
        type: DataTypes.STRING,
        allowNull: false,
        defaultValue: 0,
      },
      num_of_followers: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0,
      },
      num_of_followings: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0,
      },
      profile_img: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      banner_img: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      facebook_url: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      twitter_url: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      youtube_url: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      instagram_url: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      language: {
        type: DataTypes.STRING,
        allowNull: false,
        defaultValue: "en",
      },
      type_user: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: UserType.User,
      },
      createdAt: { type: DataTypes.DATE, defaultValue: new Date() },
      updatedAt: { type: DataTypes.DATE, defaultValue: new Date() },
    });
  },

  async down(queryInterface: QueryInterface) {
    await queryInterface.dropTable(UserModel.tableName);
  },
};
