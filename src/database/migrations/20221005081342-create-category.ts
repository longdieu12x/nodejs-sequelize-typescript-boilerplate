import { DataTypes, QueryInterface } from "sequelize";
import CategoryModel from "../../shared/models/category.model";

module.exports = {
  async up(queryInterface: QueryInterface) {
    await queryInterface.createTable(CategoryModel.tableName, {
      id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true,
      },
      name: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      createdAt: { type: DataTypes.DATE, defaultValue: new Date() },
      updatedAt: { type: DataTypes.DATE, defaultValue: new Date() },
    });
  },

  async down(queryInterface: QueryInterface) {
    await queryInterface.dropTable(CategoryModel.tableName);
  },
};
