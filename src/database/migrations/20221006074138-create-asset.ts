import { DataTypes, QueryInterface } from "sequelize";
import { AssetType, AssetStatus } from "../../components/asset/asset.enums";
import AssetModel from "../../shared/models/asset.model";
import CategoryModel from "../../shared/models/category.model";
import UserModel from "../../shared/models/user.model";
import CollectionModel from "../../shared/models/collection.model";

module.exports = {
  async up(queryInterface: QueryInterface) {
    await queryInterface.createTable(AssetModel.tableName, {
      id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true,
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false,
        defaultValue: "",
      },
      image: {
        type: DataTypes.STRING,
        allowNull: false,
        defaultValue: "",
      },
      description: {
        type: DataTypes.STRING,
        allowNull: false,
        defaultValue: "",
      },
      external_link: {
        type: DataTypes.STRING,
        allowNull: false,
        defaultValue: "",
      },
      nft_address: {
        type: DataTypes.STRING,
        allowNull: false,
        defaultValue: "",
      },
      token_id: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      amount: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      category_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        onDelete: "CASCADE",
        onUpdate: "RESTRICT",
        references: {
          model: CategoryModel.tableName,
          key: "id",
        },
      },
      collection_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        onDelete: "CASCADE",
        onUpdate: "RESTRICT",
        references: {
          model: CollectionModel.tableName,
          key: "id",
        },
      },
      creator_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        onDelete: "CASCADE",
        onUpdate: "RESTRICT",
        references: {
          model: UserModel.tableName,
          key: "id",
        },
      },
      creator_address: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      edition: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0,
      },
      ipfs: {
        type: DataTypes.STRING,
        allowNull: false,
        defaultValue: "",
      },
      metadata: {
        type: DataTypes.STRING,
        allowNull: false,
        defaultValue: "",
      },
      unlockable_content: {
        type: DataTypes.STRING,
        allowNull: false,
        defaultValue: "",
      },
      num_of_like: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0,
      },
      num_of_view: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0,
      },
      num_of_edition: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0,
      },
      num_of_claim: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0,
      },
      is_sentitive: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      is_hidden: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      type: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: AssetType.ERC721,
      },
      status: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: AssetStatus.UNLIST,
      },
      createdAt: { type: DataTypes.DATE, defaultValue: new Date() },
      updatedAt: { type: DataTypes.DATE, defaultValue: new Date() },
    });
  },

  async down(queryInterface: QueryInterface) {
    await queryInterface.dropTable(AssetModel.tableName);
  },
};
