import { DataTypes, QueryInterface } from "sequelize";
import UserModel from "../../shared/models/user.model";
import CollectionModel from "../../shared/models/collection.model";

module.exports = {
  async up(queryInterface: QueryInterface) {
    await queryInterface.createTable(CollectionModel.tableName, {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      image: {
        type: DataTypes.STRING,
      },
      banner_img: {
        type: DataTypes.STRING,
      },
      title: {
        type: DataTypes.STRING,
      },
      description: {
        type: DataTypes.STRING,
      },
      user_id: {
        type: DataTypes.INTEGER,
        onDelete: "CASCADE",
        onUpdate: "RESTRICT",
        references: {
          model: UserModel.tableName,
          key: "id",
        },
      },
      createdAt: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW,
      },
      updatedAt: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW,
      },
    });
  },
  async down(queryInterface: QueryInterface) {
    await queryInterface.dropTable("Collections");
  },
};
