import { DataTypes, QueryInterface } from "sequelize";
import FollowModel from "../../shared/models/follow.model";
import UserModel from "../../shared/models/user.model";

module.exports = {
  async up(queryInterface: QueryInterface) {
    await queryInterface.createTable(FollowModel.tableName, {
      follower_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        onDelete: "CASCADE",
        onUpdate: "RESTRICT",
        references: {
          model: UserModel.tableName,
          key: "id",
        },
        primaryKey: true,
      },
      following_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        onDelete: "CASCADE",
        onUpdate: "RESTRICT",
        references: {
          model: UserModel.tableName,
          key: "id",
        },
        primaryKey: true,
      },
      createdAt: { type: DataTypes.DATE, defaultValue: new Date() },
      updatedAt: { type: DataTypes.DATE, defaultValue: new Date() },
    });
  },

  async down(queryInterface: QueryInterface) {
    await queryInterface.dropTable(FollowModel.tableName);
  },
};
