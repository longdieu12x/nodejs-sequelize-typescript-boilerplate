import { DataTypes, QueryInterface } from "sequelize";
import UserAssetModel from "../../shared/models/user_asset.model";
import CategoryModel from "../../shared/models/category.model";
import AssetModel from "../../shared/models/asset.model";
import UserModel from "../../shared/models/user.model";

module.exports = {
  async up(queryInterface: QueryInterface) {
    await queryInterface.createTable(UserAssetModel.tableName, {
      asset_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        onDelete: "CASCADE",
        onUpdate: "RESTRICT",
        references: {
          model: AssetModel.tableName,
          key: "id",
        },
      },
      collection_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        onDelete: "CASCADE",
        onUpdate: "RESTRICT",
        references: {
          model: CategoryModel.tableName,
          key: "id",
        },
      },
      user_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        onDelete: "CASCADE",
        onUpdate: "RESTRICT",
        references: {
          model: UserModel.tableName,
          key: "id",
        },
      },
      amount: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      createdAt: { type: DataTypes.DATE, defaultValue: new Date() },
      updatedAt: { type: DataTypes.DATE, defaultValue: new Date() },
    });
  },

  async down(queryInterface: QueryInterface) {
    await queryInterface.dropTable(UserAssetModel.tableName);
  },
};
