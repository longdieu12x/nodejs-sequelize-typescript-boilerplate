import { DataTypes, QueryInterface } from "sequelize";
import UserModel from "../../shared/models/user.model";

module.exports = {
  async up(queryInterface: QueryInterface) {
    await queryInterface.bulkInsert(UserModel.tableName, [
      {
        address: "0xe42B1F6BE2DDb834615943F2b41242B172788E7E",
      },
      {
        address: "0x0deB52499C2e9F3921c631cb6Ad3522C576d5484",
      },
    ]);
  },

  async down(queryInterface: QueryInterface) {
    await queryInterface.dropTable(UserModel.tableName);
  },
};
