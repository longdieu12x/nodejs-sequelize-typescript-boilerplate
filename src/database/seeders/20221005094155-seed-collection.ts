import { DataTypes, QueryInterface } from "sequelize";
import CollectionModel from "../../shared/models/collection.model";

module.exports = {
  async up(queryInterface: QueryInterface) {
    await queryInterface.bulkInsert(CollectionModel.tableName, [
      {
        image: "",
        banner_img: "",
        title: "",
        description: "",
        user_id: 1
      },
      {
        image: "",
        banner_img: "",
        title: "",
        description: "",
        user_id: 2
      },
    ]);
  },

  async down(queryInterface: QueryInterface) {
    await queryInterface.dropTable(CollectionModel.tableName);
  }
};
