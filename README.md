* Note:
  - components: each components is a module that handle independent missions
  - database: contain config and migrations of database
  - shared: share is folder for writing stuffs that we can use in all components
    + services: will contain all files that have to interact with third party (For example: using redis, upload image to S3,...)
    + utils: will contain all files that handle data inside app (Ex: handle random number)